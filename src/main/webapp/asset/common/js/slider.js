(function () {
	var slider ='\
		<div style="background:#f2f2f2;position:absolute;top:75px;width:10%;bottom:0px">\
	    	<ol class="tree">\
	       <li>\
	          <label for="folder1" class="folderOne foldertop"><img src="common/img/时间.png" style="width:16px;height:16px" />&nbsp;资源规划和调度</label> <input type="checkbox" id="folder1" /> \
	           <ol>\
	               <li>\
	                   <label for="subfolder2" class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;集群状态信息</label> <input type="checkbox" id="subfolder2"  />\
	                   <ol> \
	                   		<li><a href="/ResourceManagement/cluster_node.html"><label  class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;CLUSTER与NODE信息 </label> <input type="checkbox" id="subfolder21"  /> </a></li>\
	                   		<li><a href="/ResourceManagement/pod_service.html"><label  class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;POD与SERVICE信息</label> <input type="checkbox" id="subfolder21"  /> </a></li>\
	                   		<li><a href="/ResourceManagement/pvc.html"><label  class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;PVC信息</label> <input type="checkbox" id="subfolder21"  /></a> </li>\
	                   </ol>\
	               </li>\
	               <li> \
	                   <a href="/ResourceManagement/add_delete.html"><label  class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;组件添加删除</label> <input type="checkbox" id="subfolder1"  /> </a>  \
	               </li>\
	               <li>\
	                   <a href="/ResourceManagement/resource_schedule_allocate.html"><label  class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;资源规划调度</label> <input type="checkbox" id="subfolder1"  /> </a> \
	               </li> \
	               <li>\
	                   <a href="/ResourceManagement/dynamic_scaling.html"><label  class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;资源动态伸缩</label> <input type="checkbox" id="subfolder1"  /> </a>  \
	               </li>\
	               <li> \
	                   <a href="/ResourceManagement/resource_estimates.html"><label  class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;资源预估</label> <input type="checkbox" id="subfolder1"  /> </a>\
	               </li> \
	           </ol> \
	       </li> \
		<li>  \
        <label for="folder2" class="folderOne" ><img src="common/img/公共-星号.png" style="width:16px;height:16px" />&nbsp;故障检测与诊断</label> <input type="checkbox" id="folder2" />   \
	        <ol>  \
	             <li>  \
	                 <label for="subfolder3" class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;集群组件监控</label> <input type="checkbox" id="subfolder3"  />\
	                 <ol>  \
	                    <li class="file folderThree"><a href="/detection/cluster_monitor.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;集群监控</a></li> \
	                    <li>\
	                    <li class="file folderThree"><a href="/detection/ioTDB_monitor.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;IoTDB监控</a></li> \
	                    <li>\
	                    <li class="file folderThree"><a href="/detection/kvStore_monitor.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;KVStore监控</a></li> \
	                    <li>\
	                    <li class="file folderThree"><a href="/detection/kingBase_monitor.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;KingBase监控</a></li> \
	                    <li>\
	                    <li class="file folderThree"><a href="/detection/inGraphDB_monitor.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;InGraphDB监控</a></li> \
	                 </ol>  \
	             </li> \
	             <li>  \
	                <label for="subfolder4" class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;规则检测</label> <input type="checkbox" id="subfolder4"  />\
	                <ol>\
	                   <li class="file folderThree"><a href="/detection/rule_configuration.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;规则配置</a></li> \
	                   <li class="file folderThree"><a href="/detection/rule_show.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;规则信息展示</a></li> \
	                </ol>\
	             </li> \
	             <li>  \
	                <label for="subfolder5" class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;日志检测</label> <input type="checkbox" id="subfolder5"  />\
	                <ol>  \
	                   <li class="file folderThree"><a href="/detection/log_show.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;日志信息展示</a></li>\
	                   <li class="file folderThree"><a href="/detection/log_configuration.html"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;日志采集配置</a></li> \
	                </ol> \
	             </li>\
	             <li>\
	                <a href="/detection/diagnosis_show.html"><label class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;故障诊断与报警</label> <input type="checkbox" id="subfolder1"  /> </a>\
	             </li> \
	        </ol>  \
	      </li> \
		<!-- ---系统调优--- -->\
		   <li>\
				<label for="folder3"  class="folderOne"><img src="common/img/file.png" style="width:16px;height:16px"/>&nbsp;系统调优</label> <input type="checkbox" id="folder3" />\
				<ol>\
					<li class="file folderThree"><a href="/HitTuning/asset/module/kv/kv.html"><i class="fa fa-circle-o"></i> KVStore调优</a></li>\
				    <li class="file folderThree"><a href="/HitTuning/asset/module/iotdb/iotdb.html"><i class="fa fa-circle-o"></i> IoTDB调优</a></li>\
				    <li class="file folderThree"><a href="/HitTuning/asset/module/spark/spark.html"><i class="fa fa-circle-o"></i> spark调优</a></li>\
				    <li class="file folderThree"><a href="/HitTuning/asset/module/rdb/rdb.html"><i class="fa fa-circle-o"></i> 关系database调优</a></li> \
			    </ol>\
		    </li>\
		   <!-- ---性能监控--- -->\
		   <li>\
			 <label for="folder4"  class="folderOne"><img src="common/img/关系数据库.png" style="width:16px;height:16px"/>&nbsp;性能监控</label> <input type="checkbox" id="folder4"/>\
			 <ol>\
				<li class="file folderThree"><a href="/monitor/"><i style="width:16px;height:16px" class="fa fa-circle-o"></i> 首页</a></li>\
				<li>\
					<label for="subfolder4r" class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;硬件资源监测</label> <input type="checkbox" id="subfolder4r"  />\
					<ol>\
					   <li class="file folderThree"><a href="/monitor/asset/module/cpu/cpuinfo.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;CPU相关</a></li>\
				       <li class="file folderThree"><a href="/monitor/asset/module/memory/memory.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;内存相关</a></li>\
				       <li class="file folderThree"><a href="/monitor/asset/module/io/io.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp; I/O相关</a></li>\
					</ol>\
			    </li>\
			    <li>\
					<label for="subfolder5r" class="folderTwo"><img src="common/img/数据库.png" style="width:16px;height:16px"/>&nbsp;数据库管理系统监控</label> <input type="checkbox" id="subfolder5r"  />\
					<ol>\
					   <li class="file folderThree"><a href="/monitor/asset/module/tsfile/tsfile.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;IoTDB监控</a></li>\
				       <li class="file folderThree"><a href="/monitor/asset/module/rdb/RDB.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;关系数据库监控</a></li>\
				       <li class="file folderThree"><a href="/monitor/asset/module/graph/graph.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp; InGraphDB监控</a></li> \
				       <li class="file folderThree"><a href="/monitor/asset/module/kv/kv.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;  KVStore监控</a></li> \
				       <li class="file folderThree"><a href="/monitor/asset/module/namenode/namenode.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp; HDFS相关</a></li>\
					</ol>\
			   </li>\
		  </ol>\
	  </li>\
   </ol>\
 </div>\
    <div style="background:#DFDCDC;position:absolute;left:10%;width:90%"></div>\	    ';
	document.write(slider);
})();