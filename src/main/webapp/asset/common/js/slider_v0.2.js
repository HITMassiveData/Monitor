var slider = '\
	<div style="background:#f2f2f2;position:absolute;top:75px;width:10%;bottom:0px">\
	<ol class="tree">\
	<!--资源规划和调度--->\
   <li>\
	<label for="folder1" class="folderOne foldertop"><img src="common/img/nwputime.png" style="width:16px;height:16px" />&nbsp;资源规划和调度</label> <input type="checkbox" id="folder1" />\
	<ol>\
	<li>\
		<label for="subfolder2" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;集群状态信息</label> <input type="checkbox" id="subfolder2"  />\
	<ol>\
		<li><a href="http://10.69.35.174:8090/ResourceManagement/cluster_node.html"><label  class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;CLUSTER与NODE信息 </label> <input type="checkbox" id="subfolder21"  /> </a></li>\
        <li><label for="subfolder21" class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/><a href="">&nbsp;POD与SERVICE信息</a></label> <input type="checkbox" id="subfolder21"  /> </li>\
               		<li><label for="subfolder21" class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;PVC信息</label> <input type="checkbox" id="subfolder21"  /> </li>\
               </ol>\
		</li>\
           <li>\
				<label for="subfolder1" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;组件添加删除</label> <input type="checkbox" id="subfolder1" /></li>\
               	<li>\
				<label for="subfolder1" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;资源规划调度</label> <input type="checkbox" id="subfolder1"  />\
				</li>\
			<li>\
				<label for="subfolder1" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;资源动态伸缩</label> <input type="checkbox" id="subfolder1"  />\
		</li>\
			<li>\
				<label for="subfolder1" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;资源预估</label> <input type="checkbox" id="subfolder1"  />\
		</li>\
		</ol>\
	</li>\
	<!-- ---故障检测与诊断--- -->\
    \
   <li>\
	<label for="folder2" class="folderOne" ><img src="common/img/nwpuPublicStar.png" style="width:16px;height:16px" />&nbsp;故障检测与诊断</label> <input type="checkbox" id="folder2" />\
		<ol>\
			<li>\
				<label for="subfolder3" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;图数据源实例1</label> <input type="checkbox" id="subfolder3"  />\
				<ol>\
				<li class="file folderThree"><a href="#"><img src="common/img/眼睛.png" style="width:16px;height:16px"/>&nbsp;系统概览</a></li>\
				<li>\
               		<label for="subfolder31" class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;元数据管理</label> <input type="checkbox" id="subfolder31"  />\
				<ol>\
                     <li class="file folderThree"><a href="#"><img src="common/img/label.png" style="width:16px;height:16px"/>&nbsp;标签信息</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/nodejs.png" style="width:16px;height:16px"/>&nbsp;节点信息</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/chart-relation.png" style="width:16px;height:16px"/>&nbsp;关系信息</a></li>\
					</ol></li>\
                   <li>\
               		<label for="subfolder32" class="folderTwo"><img src="common/img/search.png" style="width:16px;height:16px"/>&nbsp;数据查询</label> <input type="checkbox" id="subfolder32"  />\
					<ol>\
                     <li class="file folderThree"><a href="#"><img src="common/img/结构.png" style="width:16px;height:16px"/>&nbsp;产品结构</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/使用范围广.png" style="width:16px;height:16px"/>&nbsp;零件使用情况</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/compare.png" style="width:16px;height:16px"/>&nbsp;结构异同</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/物料管理.png" style="width:16px;height:16px"/>&nbsp;物料总数</a></li>\
                    </ol></li>\
				</ol>\
			</li>\
			<li>\
				<label for="subfolder4" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;图数据源实例2</label> <input type="checkbox" id="subfolder4"  />\
				<ol>\
				<li class="file folderThree"><a href="#"><img src="common/img/眼睛.png" style="width:16px;height:16px"/>&nbsp;系统概览</a></li>\
				<li>\
					<label for="subfolder41" class="folderTwo"><img src="common/img/元数据面板.png" style="width:16px;height:16px"/>&nbsp;元数据管理</label>\
					<ol>\
                     <li class="file folderThree"><a href="#"><img src="common/img/label.png" style="width:16px;height:16px"/>&nbsp;标签信息</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/nodejs.png" style="width:16px;height:16px"/>&nbsp;节点信息</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/chart-relation.png" style="width:16px;height:16px"/>&nbsp;关系信息</a></li>\
					</ol></li>\
                   <li>\
               		<label for="subfolder42" class="folderTwo"><img src="common/img/search.png" style="width:16px;height:16px"/>&nbsp;数据查询</label> <input type="checkbox" id="subfolder42"  />\
					<ol>\
                     <li class="file folderThree"><a href="#"><img src="common/img/结构.png" style="width:16px;height:16px"/>&nbsp;产品结构</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/使用范围广.png" style="width:16px;height:16px"/>&nbsp;零件使用情况</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/compare.png" style="width:16px;height:16px"/>&nbsp;结构异同</a></li>\
					<li class="file folderThree"><a href="#"><img src="common/img/物料管理.png" style="width:16px;height:16px"/>&nbsp;物料总数</a></li>\
\
                    </ol></li>\
				</ol>\
			</li>\
		</ol>\
	</li>\
	<!-- ---系统调优--- -->\
   <li>\
	<label for="folder3"  class="folderOne"><img src="common/img/file.png" style="width:16px;height:16px"/>&nbsp;系统调优</label> <input type="checkbox" id="folder3" />\
	<ol>\
	<li class="file folderThree"><a href="'+HitTuningAssetBaseURL+'module/kv/kv.html"><i class="fa fa-circle-o"></i> KVStore调优</a></li>\
    <li class="file folderThree"><a href="'+HitTuningAssetBaseURL+'module/iotdb/iotdb.html"><i class="fa fa-circle-o"></i> IoTDB调优</a></li>\
    <li class="file folderThree"><a href="'+HitTuningAssetBaseURL+'module/spark/spark.html"><i class="fa fa-circle-o"></i> spark调优</a></li>\
    <li class="file folderThree"><a href="'+HitTuningAssetBaseURL+'module/rdb/rdb.html"><i class="fa fa-circle-o"></i> 关系database调优</a></li> </ol></li>\
   <!-- ---性能监控--- -->\
   <li>\
	<label for="folder4"  class="folderOne"><img src="common/img/nwpuRDB.png" style="width:16px;height:16px"/>&nbsp;性能监控</label> <input type="checkbox" id="folder4"/>\
	<ol>\
	<li class="file folderThree"><a href="'+monitorWebBaseURL+'"><i style="width:16px;height:16px" class="fa fa-circle-o"></i> 首页</a></li>\
	<li>\
	<label for="subfolder4r" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;硬件资源监测</label> <input type="checkbox" id="subfolder4r"  />\
			<ol>\
		<li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/cpu/cpuinfo.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;CPU相关</a></li>\
       <li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/memory/memory.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;内存相关</a></li>\
       <li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/io/io.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp; I/O相关</a></li>\
			</ol>\
	   </li>\
	   <li>\
		<label for="subfolder5r" class="folderTwo"><img src="common/img/database.png" style="width:16px;height:16px"/>&nbsp;数据库管理系统监控</label> <input type="checkbox" id="subfolder5r"  />\
			<ol>\
		<li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/tsfile/tsfile.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;IoTDB监控</a></li>\
       <li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/rdb/RDB.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;关系数据库监控</a></li>\
       <li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/graph/graph.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp; InGraphDB监控</a></li> \
       <li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/kv/kv.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp;  KVStore监控</a></li> \
       <li class="file folderThree"><a href="'+monitorAssetBaseURL+'module/namenode/namenode.html"><i style="width:16px;height:16px" class="fa fa-circle-o"></i>&nbsp; HDFS相关</a></li>\
			</ol>\
	   </li>\
    \
    </ol>\
	</li>\
</ol>\
</div>\
	';
(function(){
	document.write(slider);
})();