(function(){
	var footer = '\
		<footer class="main-footer">\
			<div class="pull-right hidden-xs">\
		      <b>Version</b> 1.0.0\
		    </div>\
		    <strong>Copyright &copy; 2017-2018 <a href="http://www.hit.edu.cn">海量数据研究中心</a>.</strong> All rights\
		    reserved.\
		</footer>\
	';
	document.write(footer);
})();
