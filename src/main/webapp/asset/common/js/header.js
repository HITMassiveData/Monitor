(function () {
	var header = '\
		<nav style="background-color:#142C51" class="navbar navbar-inverse navbar-fixed-top" role="navigation">\
		<div class="container-fluid1">\
			<div class="navbar-header">\
            <img src="common/img/logo.png" alt="LOGO" style="height:75px; padding:3px 15px 3px;">\
            <font size="6" color="white" face="Microsoft YaHei" style="vertical-align: middle;">面向高端制造领域的大数据管理系统</font>\
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">\
					<span class="sr-only">Toggle navigation</span>\
					<span class="icon-bar"></span>\
					<span class="icon-bar"></span>\
					<span class="icon-bar"></span>\
				</button>\
				<!--<a class="navbar-brand" href="#"><span>Lumino</span>Admin</a>-->\
                <table style="margin-top: 28px;float: right">\
	                <tr class="headback">\
        	        	<td bgcolor="#142C51" style="border:hidden;border-right: solid;border-right-color: #797979;padding: 0 10px 0 10px;"><font class="head-menu" color="#FFFFFF" size="3px" ><a href="http://166.111.80.119:18080/MIXServerV7/MIXMain">首页</a></font></td>\
    	                <td bgcolor="#142C51" style="border-right: solid;border-right-color: #797979;padding: 0 10px 0 10px;"><font color="#FFFFFF" size="3px" class="head-menu"><a href="http://166.111.80.119:18080/MIXServerV7/DatasouceEntry">数据源管理</a></font></td>\
	                    <td bgcolor="#142C51" style="border-right: solid;border-right-color: #797979;padding: 0 10px 0 10px;"><font color="#FFFFFF" size="3px" class="head-menu"><a href="http://166.111.80.119:18080/MIXServerV7/IGMain">一体化管理引擎</a></font></td>\
                    	<td bgcolor="#142C51" style="border-right: solid;border-right-color: #797979;padding: 0 10px 0 10px;"><font color="#FFFFFF" style="padding: 0 10px 0 10px;" size="5px" class="head-menu"><a href="http://101.6.240.89:18090/ResourceManagement/cluster_node.html">运维工具</a></font></td>\
		                <td bgcolor="#142C51"><font color="#FFFFFF" style="padding: 0 10px 0 10px;" size="3px" class="head-menu"><a href="http://166.111.80.119:18080/MIXServerV7/Benchmark.jsp">测试工具</a></font></td>\
    	            </tr>\
                </table>\
			</div>\
		</div><!-- /.container-fluid -->\
	</nav>		\	';

	document.write(header);
})();
