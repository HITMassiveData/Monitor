var slider = '\
	  <aside class="main-sidebar">\
	    <!-- sidebar: style can be found in sidebar.less -->\
	    <section class="sidebar">\
	      <!-- sidebar menu: : style can be found in sidebar.less -->\
	      <ul class="sidebar-menu">\
	        <li class="header">功能导航</li>\
	        <li class="active treeview">\
	          <a href="javascript:void(0)">\
	            <i class="fa fa-dashboard"></i> <span>整体运行概览</span>\
	          </a>\
	          <ul class="treeview-menu">\
	            <li><a href="'+monitorWebBaseURL+'"><i class="fa fa-circle-o"></i> 首页</a></li>\
	          </ul>\
	        </li>\
	        <li class="treeview active">\
	          <a href="javascript:void(0)">\
	            <i class="fa fa-files-o"></i>\
	            <span>硬件资源监测</span>\
	          </a>\
	          <ul class="treeview-menu">\
	            <li><a href="'+monitorAssetBaseURL+'module/cpu/cpuinfo.html"><i class="fa fa-circle-o"></i> CPU相关</a></li>\
	            <li><a href="'+monitorAssetBaseURL+'module/memory/memory.html"><i class="fa fa-circle-o"></i> 内存相关</a></li>\
	            <li><a href="'+monitorAssetBaseURL+'module/io/io.html"><i class="fa fa-circle-o"></i> I/O相关</a></li>\
	          </ul>\
	        </li>\
	        <li class="treeview active">\
	          <a href="javascript:void(0)">\
	            <i class="fa fa-database"></i>\
	            <span>数据库管理系统监控</span>\
	          </a>\
	          <ul class="treeview-menu">\
	          <li><a href="module/tsfile/tsfile.html"><i class="fa fa-circle-o"></i> IoTDB监控</a></li>\
	        	<li><a href="module/rdb/RDB.html"><i class="fa fa-circle-o"></i> 关系数据库监控</a></li>\
	        	<li><a href="module/graph/graph.html"><i class="fa fa-circle-o"></i> InGraphDB监控</a></li>\
	        	<li><a href="module/kv/kv.html"><i class="fa fa-circle-o"></i> KVStore监控</a></li>\
	            <li><a href="'+monitorAssetBaseURL+'module/namenode/namenode.html"><i class="fa fa-circle-o"></i> HDFS相关</a></li>\
	          </ul>\
	        </li>\
	        <li class="treeview active">\
	          <a href="javascript:void(0)">\
	            <i class="fa fa-pie-chart"></i>\
	            <span>集群监测</span>\
	          </a>\
	          <ul class="treeview-menu">\
	            <li><a href="module/docker/docker.html"><i class="fa fa-circle-o"></i> docker监控</a></li>\
	          </ul>\
	        </li>\
	      </ul>\
	    </section>\
	    <!-- /.sidebar -->\
	  </aside>\
	';
(function(){
	document.write(slider);
})();
