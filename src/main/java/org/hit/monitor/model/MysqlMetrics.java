package org.hit.monitor.model;

public class MysqlMetrics {
	public String  Variable_name;
	public String  Value;
	
	
	public String getVariable_name() {
		return Variable_name;
	}
	public void setVariable_name(String variable_name) {
		Variable_name = variable_name;
	}
	public String getValue() {
		return Value;
	}
	public void setValue(String value) {
		Value = value;
	}
	
}
